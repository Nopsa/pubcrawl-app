package com.mobDevProject.pubcrawlapp;

import java.util.ArrayList;
import java.util.List;

public class ChallengeSetup {
	private static final String TITLE_1 = "The angry moskus.";
	private static final String TEXT_1 = "Drink three vodka Red Bull in 30 minutes or less.";
	private static final int DIFFICULTY_1 = ChallengeHandler.HARD;
	private static final int GENDER_1 = ChallengeHandler.BOTH;
	private static final int XP_GAIN_1 = 100;

	private static final String TITLE_2 = "The classic.";
	private static final String TEXT_2 = "It's been a hard week hasent it? Drink a beer and relax.";
	private static final int DIFFICULTY_2 = ChallengeHandler.EASY;
	private static final int GENDER_2 = ChallengeHandler.BOTH;
	private static final int XP_GAIN_2 = 20;

	private static final String TITLE_3 = "CHUG! CHUG! CHUG!.";
	private static final String TEXT_3 = "Drink a beer in less than 30 seconds, then realize what a waste it was.";
	private static final int DIFFICULTY_3 = ChallengeHandler.MEDIUM;
	private static final int GENDER_3 = ChallengeHandler.BOTH;
	private static final int XP_GAIN_3 = 50;
	
	private static final String TITLE_4 = "The Bartender's Special.";
	private static final String TEXT_4 = "...";
	private static final int DIFFICULTY_4 = ChallengeHandler.EASY;
	private static final int GENDER_4 = ChallengeHandler.BOTH;
	private static final int XP_GAIN_4 = 20;

	public List<ChallengeData> createChallenges() {
		List<ChallengeData> challenges = new ArrayList<ChallengeData>();

		ChallengeData data = new ChallengeData();
		data.setDifficulty(DIFFICULTY_1);
		data.setGender(GENDER_1);
		data.setText(TEXT_1);
		data.setXpGain(XP_GAIN_1);
		data.setTitle(TITLE_1);
		challenges.add(data);

		data = new ChallengeData();
		data.setDifficulty(DIFFICULTY_2);
		data.setGender(GENDER_2);
		data.setText(TEXT_2);
		data.setXpGain(XP_GAIN_2);
		data.setTitle(TITLE_2);
		challenges.add(data);

		data = new ChallengeData();
		data.setDifficulty(DIFFICULTY_3);
		data.setGender(GENDER_3);
		data.setText(TEXT_3);
		data.setXpGain(XP_GAIN_3);
		data.setTitle(TITLE_3);
		challenges.add(data);
		
		data = new ChallengeData();
		data.setDifficulty(DIFFICULTY_4);
		data.setGender(GENDER_4);
		data.setText(TEXT_4);
		data.setXpGain(XP_GAIN_4);
		data.setTitle(TITLE_4);
		challenges.add(data);

		return challenges;
	}
}
