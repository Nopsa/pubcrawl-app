package com.mobDevProject.pubcrawlapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class StatsActivity extends Activity {
	private PubCrawlAccount account;
	private ProgressBar progress;
	TextView userName;
	TextView userGender;
	TextView userLevel;
	TextView userXp;
	ImageView userAvatar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stats);

		Bundle data = getIntent().getExtras();
		this.account = (PubCrawlAccount) data.getParcelable("pubCrawlAccount");

		this.userAvatar();
		this.xpBar();
		this.userName();
		this.userLevel();
		this.userXp();
	}

	private void userLevel() {
		this.userLevel = (TextView) findViewById(R.id.userLevel);
		this.userLevel.setText("Level " + this.account.getLevel());
	}

	private void userXp() {
		this.userXp = (TextView) findViewById(R.id.userXp);
		this.userXp.setText("XP: " + this.account.getXp() + " / "
				+ this.account.getRequiredXpToLevel());
	}

	public void userName() {
		this.userName = (TextView) findViewById(R.id.userName);
		this.userName.setText(this.account.getAccountName());
	}

	/**
	 * Calculate the percentage of xp gained in current level and displays it on
	 * a progress bar.
	 */
	public void xpBar() {
		this.progress = (ProgressBar) findViewById(R.id.progressBar1);

		float xp = this.account.getXp();
		float xpRequiredToLevelUp = this.account.getRequiredXpToLevel();
		float currentProgress = (xp / xpRequiredToLevelUp) * 100;

		this.progress.setProgress((int) currentProgress);
	}

	/**
	 * Chooses a image based on the users gender
	 * 
	 * TODO: Add more images based on user level.
	 */
	public void userAvatar() {
		this.userAvatar = (ImageView) findViewById(R.id.userAvatar);

		if (this.account.getGender().matches("male")) {
			this.userAvatar
					.setBackgroundResource(R.drawable.button_male_viking);
		} else {
			this.userAvatar
					.setBackgroundResource(R.drawable.button_female_viking);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stats, menu);
		return true;
	}

}
