package com.mobDevProject.pubcrawlapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class ChallengeActivity extends Activity {
	private ChallengeHandler challengeHandler;
	private ChallengeData challenge;

	private TextView challengeTitle;
	private TextView challengeText;
	private TextView challengeXpGain;

	private Button hardChallengeButton;
	private Button randomChallengeButton;
	private Button easyChallengeButton;
	private Button mediumChallengeButton;

	private Button finishedChallengeButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenge);

		this.finishedChallengeButton = (Button) findViewById(R.id.finish_button);
		this.finishedChallengeButton.setEnabled(false);

		this.challengeTitle = (TextView) findViewById(R.id.challenge_title_text);
		this.challengeText = (TextView) findViewById(R.id.challenge_text_string);
		this.challengeXpGain = (TextView) findViewById(R.id.challenge_xp_string);

		Bundle bundleData = getIntent().getExtras();
		this.challengeHandler = (ChallengeHandler) bundleData
				.getParcelable("challengeHandler");

		this.challenge = null;
		this.loadSavedPreferences();

		if (this.challenge != null) {
			this.showCurrentChallenge();
			this.finishedChallengeButton.setEnabled(true);
		}

		this.enableInputListeners();
	}

	/**
	 * Set the output text to the current challenge data.
	 */
	private void showCurrentChallenge() {
		this.challengeTitle.setText("Title: " + this.challenge.getTitle());
		this.challengeText.setText(this.challenge.getText());
		this.challengeXpGain.setText("Xp: " + this.challenge.getXpGain());

		this.finishedChallengeButton.setEnabled(true);
		this.showText();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.challenge, menu);
		return true;
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onStop();

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		this.savePreferences();
	}

	/**
	 * Save current challenge to a shared preference.
	 */
	private void savePreferences() {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = sharedPreferences.edit();

		if (this.isValidChallenge(this.challenge) == true) {
			editor.putString("challengeTitle", this.challenge.getTitle());
			editor.putString("challengeText", this.challenge.getText());
			editor.putInt("challengeXpGain", this.challenge.getXpGain());
			editor.putInt("challengeDifficulty", this.challenge.getDifficulty());
			editor.putInt("challengeGender", this.challenge.getGender());

			editor.commit();
		}
	}

	/**
	 * Checks if a challenge has appropriate variables.
	 * 
	 * @param challengeToCheck
	 *            The challenge to check if is valid.
	 * @return true if it is a valid challenge, else false.
	 */
	private boolean isValidChallenge(ChallengeData challengeToCheck) {
		if (challengeToCheck == null) {
			return false;
		}

		if (challengeToCheck.getTitle() == null) {
			return false;
		}

		if (challengeToCheck.getText() == null) {
			return false;
		}

		if (challengeToCheck.getXpGain() == -1) {
			return false;
		}

		if (challengeToCheck.getDifficulty() == -1) {
			return false;
		}

		if (challengeToCheck.getGender() == -1) {
			return false;
		}

		return true;
	}

	/**
	 * Load a challenge saved in a shared preference.
	 */
	private void loadSavedPreferences() {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);

		ChallengeData loadChallenge = new ChallengeData();

		loadChallenge.setTitle(sharedPreferences.getString("challengeTitle",
				null));
		loadChallenge.setText(sharedPreferences
				.getString("challengeText", null));
		loadChallenge
				.setXpGain(sharedPreferences.getInt("challengeXpGain", -1));
		loadChallenge.setDifficulty(sharedPreferences.getInt(
				"challengeDifficulty", -1));
		loadChallenge
				.setGender(sharedPreferences.getInt("challengeGender", -1));

		if (this.isValidChallenge(loadChallenge) == true) {
			this.challenge = loadChallenge;
		}
	}

	/**
	 * Show the text on the device.
	 */
	private void showText() {
		this.challengeTitle.setVisibility(View.VISIBLE);
		this.challengeText.setVisibility(View.VISIBLE);
		this.challengeXpGain.setVisibility(View.VISIBLE);
	}

	/**
	 * Get a random hard challenge and display it on the device.
	 */
	private void displayHardChallenge() {
		this.challenge = this.challengeHandler.getRandomHardChallenge();
		this.showCurrentChallenge();
	}

	/**
	 * Get a random medium challenge and display it on the device.
	 */
	private void displayMediumChallenge() {
		this.challenge = this.challengeHandler.getRandomMediumChallenge();
		this.showCurrentChallenge();
	}

	/**
	 * Get a random easy challenge and display it on the device.
	 */
	private void displayEasyChallenge() {
		this.challenge = ChallengeActivity.this.challengeHandler
				.getRandomEasyChallenge();
		this.showCurrentChallenge();
	}

	/**
	 * Get a random challenge from a random difficulty and display it on the
	 * device.
	 */
	private void displayRandomChallenge() {
		this.challenge = ChallengeActivity.this.challengeHandler
				.getRandomChallenge();
		this.showCurrentChallenge();
	}

	/**
	 * Create listeners for the buttons.
	 */
	private void enableInputListeners() {
		this.hardChallengeButton = (Button) findViewById(R.id.challenge_hard_button);
		this.hardChallengeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ChallengeActivity.this.displayHardChallenge();
			}
		});

		this.randomChallengeButton = (Button) findViewById(R.id.challenge_random_button);
		this.randomChallengeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ChallengeActivity.this.displayRandomChallenge();
			}
		});

		this.easyChallengeButton = (Button) findViewById(R.id.challenge_easy_button);
		this.easyChallengeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ChallengeActivity.this.displayEasyChallenge();
			}
		});

		this.mediumChallengeButton = (Button) findViewById(R.id.challenge_medium_button);
		this.mediumChallengeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ChallengeActivity.this.displayMediumChallenge();
			}
		});

		this.finishedChallengeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				ChallengeActivity.this.challengeCompleted();
			}
		});
	}

	/**
	 * Remove challenge from shared preference and go back to MainFragment
	 * sending the challenge data.
	 */
	private void challengeCompleted() {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = sharedPreferences.edit();

		editor.remove("challengeTitle");
		editor.remove("challengeText");
		editor.remove("challengeXpGain");
		editor.remove("challengeDifficulty");
		editor.remove("challengeGender");

		editor.commit();

		Intent returnIntent = new Intent();
		returnIntent.putExtra("challenge", this.challenge);
		setResult(RESULT_OK, returnIntent);
		finish();
	}
}
