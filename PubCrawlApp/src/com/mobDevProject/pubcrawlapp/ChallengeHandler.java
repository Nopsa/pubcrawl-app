package com.mobDevProject.pubcrawlapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class ChallengeHandler implements Parcelable {

	private static final String TAG = "ChallengeHandler";

	// Gender specific.
	static final int MALE = 0;
	static final int FEMALE = 1;
	static final int BOTH = 2;

	// Challenge difficulty.
	static final int EASY = 0;
	static final int MEDIUM = 1;
	static final int HARD = 2;

	private List<ChallengeData> easyChallenge = null;
	private List<ChallengeData> mediumChallenge = null;
	private List<ChallengeData> hardChallenge = null;

	private Random random = null;

	public ChallengeHandler() {
		this.random = new Random();

		this.easyChallenge = new ArrayList<ChallengeData>();
		this.mediumChallenge = new ArrayList<ChallengeData>();
		this.hardChallenge = new ArrayList<ChallengeData>();
	}

	public ChallengeHandler(Parcel in) {
		this.random = new Random();

		this.easyChallenge = new ArrayList<ChallengeData>();
		this.mediumChallenge = new ArrayList<ChallengeData>();
		this.hardChallenge = new ArrayList<ChallengeData>();

		in.readTypedList(this.easyChallenge, ChallengeData.CREATOR);
		in.readTypedList(this.mediumChallenge, ChallengeData.CREATOR);
		in.readTypedList(this.hardChallenge, ChallengeData.CREATOR);
	}

	public void addChallenge(ChallengeData data) {
		if (data.difficulty == EASY) {
			this.easyChallenge.add(data);
		}
		
		if (data.difficulty == MEDIUM) {
			this.mediumChallenge.add(data);
		}

		if (data.difficulty == HARD) {
			this.hardChallenge.add(data);
		}
	}

	public ChallengeData getRandomChallenge() {
		int randomchallengeDifficulty = this.random.nextInt(3);
		ChallengeData data = null;

		if (randomchallengeDifficulty == EASY) {
			data = this.getRandomEasyChallenge();
		}

		if (randomchallengeDifficulty == MEDIUM) {
			data = this.getRandomMediumChallenge();
		}

		if (randomchallengeDifficulty == HARD) {
			data = this.getRandomHardChallenge();
		}

		if (data == null) {
			if (this.easyChallenge.size() > 0) {
				data = this.getRandomEasyChallenge();
				return data;
			}

			if (this.mediumChallenge.size() > 0) {
				data = this.getRandomMediumChallenge();
				return data;
			}

			if (this.hardChallenge.size() > 0) {
				data = this.getRandomHardChallenge();
				return data;
			}
		}

		return data;
	}

	public ChallengeData getRandomEasyChallenge() {
		ChallengeData data = null;

		if (this.easyChallenge.size() > 0) {
			int randomIndex = this.random.nextInt(this.easyChallenge.size());
			data = this.easyChallenge.get(randomIndex);
		}

		return data;
	}

	public ChallengeData getRandomMediumChallenge() {
		ChallengeData data = null;

		if (this.mediumChallenge.size() > 0) {
			int randomIndex = this.random.nextInt(this.mediumChallenge.size());
			data = this.mediumChallenge.get(randomIndex);
		}

		return data;
	}

	public ChallengeData getRandomHardChallenge() {
		ChallengeData data = null;

		if (this.hardChallenge.size() > 0) {
			int randomIndex = this.random.nextInt(this.hardChallenge.size());
			data = this.hardChallenge.get(randomIndex);
		}

		return data;
	}

	/**
	 * 
	 * 
	 * @param dbHelper
	 *            the database helper
	 * @param gender
	 *            the account gender.
	 */
	public void save(DatabaseHelper dbHelper, List<ChallengeData> data) {

		final ContentValues values = new ContentValues();
		final SQLiteDatabase db = dbHelper.getReadableDatabase();

		for (ChallengeData challenge : data) {
			values.put(DatabaseHelper.CHALLENGE_TITLE, challenge.getTitle());
			values.put(DatabaseHelper.CHALLENGE_TEXT, challenge.getText());
			values.put(DatabaseHelper.CHALLENGE_XP_GAIN, challenge.getXpGain());
			values.put(DatabaseHelper.CHALLENGE_GENDER_SPECIFIC,
					challenge.getGender());
			values.put(DatabaseHelper.CHALLENGE_DIFFICULTY,
					challenge.getDifficulty());

			db.insert(DatabaseHelper.CHALLENGE_TABLE_NAME, null, values);
		}
		db.close();
	}

	public boolean load(final DatabaseHelper dbHelper, final int accountGender) {
		final SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor;

		cursor = db.query(DatabaseHelper.CHALLENGE_TABLE_NAME, new String[] {
				DatabaseHelper.CHALLENGE_ID, DatabaseHelper.CHALLENGE_TITLE,
				DatabaseHelper.CHALLENGE_TEXT,
				DatabaseHelper.CHALLENGE_XP_GAIN,
				DatabaseHelper.CHALLENGE_GENDER_SPECIFIC,
				DatabaseHelper.CHALLENGE_DIFFICULTY }, null, null, null, null,
				null);
		cursor.moveToFirst();

		if (cursor == null || cursor.getCount() <= 0) {
			Log.d(TAG, "Table " + DatabaseHelper.CHALLENGE_TABLE_NAME
					+ " doest not exist in the database");

			return false;
		} else {
			Log.d(TAG, "Table " + DatabaseHelper.CHALLENGE_TABLE_NAME
					+ " allready exists in the database");

			String title = null;
			String text = null;
			int xpGain;
			int genderSpecific;
			int difficulty;

			while (!cursor.isAfterLast()) {
				genderSpecific = cursor
						.getInt(cursor
								.getColumnIndex(DatabaseHelper.CHALLENGE_GENDER_SPECIFIC));

				if (genderSpecific == BOTH || accountGender == genderSpecific) {
					title = cursor.getString(cursor
							.getColumnIndex(DatabaseHelper.CHALLENGE_TITLE));
					text = cursor.getString(cursor
							.getColumnIndex(DatabaseHelper.CHALLENGE_TEXT));
					xpGain = cursor.getInt(cursor
							.getColumnIndex(DatabaseHelper.CHALLENGE_XP_GAIN));
					difficulty = cursor
							.getInt(cursor
									.getColumnIndex(DatabaseHelper.CHALLENGE_DIFFICULTY));

					ChallengeData data = new ChallengeData();
					data.setTitle(title);
					data.setText(text);
					data.setXpGain(xpGain);
					data.setGender(genderSpecific);
					data.setDifficulty(difficulty);

					this.addChallenge(data);

					cursor.moveToNext();
				}
			}
		}

		cursor.close();

		return true;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		dest.writeTypedList(this.easyChallenge);
		dest.writeTypedList(this.mediumChallenge);
		dest.writeTypedList(this.hardChallenge);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public ChallengeHandler createFromParcel(Parcel in) {
			return new ChallengeHandler(in);
		}

		public ChallengeHandler[] newArray(int size) {
			return new ChallengeHandler[size];
		}
	};
}
