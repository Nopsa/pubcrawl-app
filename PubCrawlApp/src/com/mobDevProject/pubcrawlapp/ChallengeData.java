package com.mobDevProject.pubcrawlapp;

import android.os.Parcel;
import android.os.Parcelable;

public class ChallengeData implements Parcelable {
	private String text = null;
	private String title = null;
	private int xpGain;
	int gender;
	int difficulty;

	public ChallengeData() {
	}

	ChallengeData(String title, String text, int xpGain, int difficulty,
			int gender) {
		this.title = title;
		this.text = text;
		this.xpGain = xpGain;
		this.difficulty = difficulty;
		this.gender = gender;
	}

	public ChallengeData(Parcel in) {
		this.title = in.readString();
		this.text = in.readString();
		this.xpGain = in.readInt();
		this.difficulty = in.readInt();
		this.gender = in.readInt();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getXpGain() {
		return xpGain;
	}

	public void setXpGain(int xpGain) {
		this.xpGain = xpGain;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(int difficulty) {
		this.difficulty = difficulty;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.title);
		dest.writeString(this.text);
		dest.writeInt(this.xpGain);
		dest.writeInt(this.difficulty);
		dest.writeInt(this.gender);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public ChallengeData createFromParcel(Parcel in) {
			return new ChallengeData(in);
		}

		public ChallengeData[] newArray(int size) {
			return new ChallengeData[size];
		}
	};
}
