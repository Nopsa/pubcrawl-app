package com.mobDevProject.pubcrawlapp;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;

public class MainFragment extends Fragment {

	private UiLifecycleHelper uiHelper;
	private Intent intent;
	private PubCrawlAccount account;
	private ChallengeHandler challenges;
	private DatabaseHelper dbHelper;
	private ChallengeData challenge = null;
	private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	private boolean pendingPublishReauthorization = false;
	private static final String TAG = "MainFragment";
	private boolean loggedInToFacebook = false;
	private boolean tabletOrPhone;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.uiHelper = new UiLifecycleHelper(getActivity(), callback);
		this.uiHelper.onCreate(savedInstanceState);

		this.account = new PubCrawlAccount();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    View view = inflater.inflate(R.layout.activity_main, container, false);
	    
	    
	    this.tabletOrPhone = getResources().getBoolean(R.bool.isTablet);
	    
	    // Facebook log in/log out button.
	    if(this.tabletOrPhone) {
	    LoginButton authButtonTablet = (LoginButton) view.findViewById(R.id.authButtonTablet);
		authButtonTablet.setFragment(this);
	    } else {
		LoginButton authButtonPhone = (LoginButton) view.findViewById(R.id.authButtonPhone);
		authButtonPhone.setFragment(this);
	    }
	    
	    if (savedInstanceState != null) {
	        pendingPublishReauthorization = savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
	    }
	    return view;
	}
	
	
	private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	  
		// Checks if you are logged into facebook.
		if (state.isOpened()) {
	        Log.i(TAG, "Logged in...");
	        loggedInToFacebook = true;
		
	        // Checks if you are allowed to publish challenge to facebook.
	        if (pendingPublishReauthorization && state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
	            pendingPublishReauthorization = false;
	            publishChallenge();
	        }
	    } else if (state.isClosed()) {
	        Log.i(TAG, "Logged out...");
	        loggedInToFacebook = false;
	    }
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public void onStart() {
		super.onStart();

		this.setupAccount();
		this.setupChallenges();

		this.tabletOrPhone = getResources().getBoolean(R.bool.isTablet);
		
		// Checks if you are using a tablet or phone.
		if (this.tabletOrPhone) {
			buttonsForTablet();
		} else {
			buttonsForPhone();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}

		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);

		if (requestCode == 1) {
			if (resultCode == Activity.RESULT_OK) {
				this.account = data.getExtras().getParcelable("account");

				Log.d(TAG, "Saving account to database");
				this.account.save(this.dbHelper);

				Log.d(TAG, "Saved as " + this.account.getAccountName() + " "
						+ this.account.getGender());
			}
			if (resultCode == Activity.RESULT_CANCELED) {
				// Write your code if there's no result
			}
		}

		if (requestCode == 2) {
			if (resultCode == Activity.RESULT_OK) {

				if (this.account == null)
					return;

				ChallengeData challenge = data.getExtras().getParcelable("challenge");
	
				this.challenge = challenge;
				
				Log.d(TAG,"Received " + challenge.getTitle() + " From challenges");
				int level = this.account.getLevel();
				this.account.addXp(challenge.getXpGain());
				int updatedLevel = this.account.getLevel();
					
				this.account.update(this.dbHelper);
				
				if(loggedInToFacebook == true) {
					((MainActivity)getActivity()).showNoticeDialog();
				}

				this.account.update(this.dbHelper);

			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
		uiHelper.onSaveInstanceState(outState);
	}

	public void setupAccount() {
		// this.account = new PubCrawlAccount();
		this.dbHelper = new DatabaseHelper(getActivity());

		if (this.account.load(dbHelper) == false) {
			this.intent = new Intent(getActivity(), AccountSetupActivity.class);
			this.intent.putExtra("pubCrawlAccount", account);

			startActivityForResult(this.intent, 1);
		} else {
			Log.d(TAG, "Table for account exists in the database");
		}
	}

	public void setupChallenges() {

		if (this.account.getGender() == null) {
			return;
		}

		// this.challenges = new ChallengeHandler();
		this.dbHelper = new DatabaseHelper(getActivity());

		int accountGender = -1;

		if (this.account.getGender().equals("male")) {
			accountGender = ChallengeHandler.MALE;
		}

		if (this.account.getGender().equals("female")) {
			accountGender = ChallengeHandler.FEMALE;
		}

		if (accountGender != -1) {
			this.challenges = new ChallengeHandler();

			if (this.challenges.load(dbHelper, accountGender) == false) {
				// this.intent = new Intent(getActivity(),
				// ChallangeSetup.class);
				// this.intent.putExtra("challengeHandler", challenges);

				ChallengeSetup setup = new ChallengeSetup();
				List<ChallengeData> data = setup.createChallenges();

				this.challenges.save(this.dbHelper, data);

				this.challenges.load(dbHelper, accountGender);
			} else {
				Log.d(TAG, "Table for challanges exists in the database");
			}
		}
	}
	
	
	// The method will first check if the logged-in user has granted your app publish permissions; 
	// if they have not, they will be prompted to reauthorize the app and grant the missing permissions.
	public void publishChallenge() {
	    Session session = Session.getActiveSession();

	    if (session != null) {

	        // Check for publish permissions    
	        List<String> permissions = session.getPermissions();
	        if (!isSubsetOf(PERMISSIONS, permissions)) {
	            pendingPublishReauthorization = true;
	            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSIONS);
	            session.requestNewPublishPermissions(newPermissionsRequest);
	            return;
	        }
	        
	        // If there is a valid challenge, publish it.
	        if(this.challenge != null) {
	        	Bundle postParams = new Bundle();
	        	postParams.putString("name", "Valhalla Run Challenge");
	        	postParams.putString("caption", this.account.getAccountName() + " completed the " + 
	        						  this.challenge.getTitle() + " challenge.");
	        	postParams.putString("description","Description: " + this.challenge.getText()+ "." + 
	        						 " Gained: " + this.challenge.getXpGain() + " xp.");
	        	postParams.putString("link", "http://suttungdigital.com");
	        	postParams.putString("picture", "https://bitbucket.org/Nopsa/pubcrawl-app/raw/b7a651daa2c7b03bddcdcdec7e09ad0d5e750248/assets/ValhallaRunImg.jpg");
	  
				Request.Callback callback = new Request.Callback() {

					public void onCompleted(Response response) {
						JSONObject graphResponse = response.getGraphObject().getInnerJSONObject();

						String postId = null;
						try {
							postId = graphResponse.getString("id");
						} catch (JSONException e) {
							Log.i(TAG, "JSON error " + e.getMessage());
						}
						FacebookRequestError error = response.getError();
						if (error != null) {
							Toast.makeText(getActivity().getApplicationContext(),error.getErrorMessage(), Toast.LENGTH_SHORT).show();
						} else {
							Toast.makeText(getActivity().getApplicationContext(), postId, Toast.LENGTH_LONG).show();
						}
					}
				};

				// Request object that will be executed by a subclass
				// RequestAsyncTask.
				Request request = new Request(session, "me/feed", postParams, HttpMethod.POST, callback);

				RequestAsyncTask task = new RequestAsyncTask(request);
				task.execute();
			}
		}
	}
	
	
	
	//	Determines whether or not the user has granted the necessary permissions to 
	//	publish the story.
	private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
	    for (String string : subset) {
	        if (!superset.contains(string)) {
	            return false;
	        }
	    }
	    return true;
	}

	
	// Button for tablets.
	public void buttonsForTablet() {
		
		ImageView imageChallengesTablet = (ImageView) getView().findViewById(R.id.xImageChallenges);

		imageChallengesTablet.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent(getActivity(), ChallengeActivity.class);
				intent.putExtra("challengeHandler", challenges);
				startActivityForResult(intent, 2);
			}
		});

		ImageView imageMapTablet = (ImageView) getView().findViewById(R.id.xImageMap);

		imageMapTablet.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent(getActivity(), Map.class);
				startActivity(intent);
			}
		});

		ImageView imageStatsTablet = (ImageView) getView().findViewById(R.id.xImageStats);

		imageStatsTablet.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent(getActivity(), StatsActivity.class);
				intent.putExtra("pubCrawlAccount", account);
				startActivity(intent);
			}
		});

	}

	
	// Buttons for phones.
	public void buttonsForPhone() {
		ImageView imageChallengesPhone = (ImageView) getView().findViewById(R.id.imageChallenges);

		imageChallengesPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent(getActivity(), ChallengeActivity.class);
				intent.putExtra("challengeHandler", challenges);
				startActivityForResult(intent, 2);
			}
		});

		ImageView imageMapPhone = (ImageView) getView().findViewById(R.id.imageMap);

		imageMapPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent(getActivity(), Map.class);
				startActivity(intent);
			}
		});

		ImageView imageStatsPhone = (ImageView) getView().findViewById(R.id.imageStats);

		imageStatsPhone.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				intent = new Intent(getActivity(), StatsActivity.class);
				intent.putExtra("pubCrawlAccount", account);
				startActivity(intent);
			}
		});

	}
}
