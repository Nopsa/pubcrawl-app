package com.mobDevProject.pubcrawlapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class PubCrawlAccount implements Parcelable {
	private LevelHandler lvlHandler;
	private long id;
	private String accountName;
	private String gender;

	private static final String TAG = "PubCrawlAccount";

	public PubCrawlAccount() {
		this.accountName = null;
		this.gender = null;
		this.lvlHandler = new LevelHandler(1, 0);
	}

	public PubCrawlAccount(Parcel in) {
		String[] data = new String[3];

		in.readStringArray(data);
		this.id = Long.parseLong(data[0]);
		this.accountName = data[1];
		this.gender = data[2];

		int xp;
		int level;

		xp = in.readInt();
		level = in.readInt();
		this.lvlHandler = new LevelHandler(level, xp);
	}

	public PubCrawlAccount(String accountName, String gender) {
		this.accountName = accountName;
		this.gender = gender;

		this.lvlHandler = new LevelHandler(1, 0);
	}

	/**
	 * Add experience points to the user
	 * 
	 * @param xp Experience points to add.
	 */
	public void addXp(int xp) {
		this.lvlHandler.addXp(xp);
	}

	/**
	 * @return The accounts gender.
	 */
	public String getGender() {
		return this.gender;
	}

	/**
	 * @return The accounts user name.
	 */
	public String getAccountName() {
		return this.accountName;
	}

	/**
	 * @return The users current experience points.
	 */
	public int getXp() {
		return this.lvlHandler.getXp();
	}

	/**
	 * @return The users current level.
	 */
	public int getLevel() {
		return this.lvlHandler.getLevel();
	}

	/**
	 * @return The experience points required to level up.
	 */
	public int getRequiredXpToLevel() {
		return this.lvlHandler.getRequiredXpToLevel();
	}

	/**
	 * @param gender The account gender.
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @param accountName The users account name.
	 */
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	/**
	 * @param xp Experience points.
	 */
	public void setXp(int xp) {
		this.lvlHandler.setXp(xp);
	}

	/**
	 * Saves the user account to a table in a database.
	 * 
	 * @param dbHelper
	 *            the database helper we are using.
	 */
	public void save(DatabaseHelper dbHelper) {
		Log.d(TAG, "save(Database dbHelper)");

		if (this.accountName == null || this.gender == null) {
			Log.d(TAG, "Cant save to DB, Account is " + this.accountName
					+ " Gender is " + this.gender);
			return;
		}

		final ContentValues values = new ContentValues();

		Log.d(TAG, "Putting accountName");
		values.put(DatabaseHelper.ACCOUNT_NAME, this.accountName);

		Log.d(TAG, "Putting gender");
		values.put(DatabaseHelper.ACCOUNT_GENDER, this.gender);

		Log.d(TAG, "Putting lvlHandler.getXp() : " + this.getXp());
		values.put(DatabaseHelper.ACCOUNT_XP, this.getXp());

		Log.d(TAG, "Putting lvlHandler.getLevel()");
		values.put(DatabaseHelper.ACCOUNT_LEVEL, this.getLevel());

		Log.d(TAG, "Getting readable database");
		final SQLiteDatabase db = dbHelper.getWritableDatabase();

		Log.d(TAG, "Inserting into database");
		this.id = db.insert(DatabaseHelper.ACCOUNT_TABLE_NAME, null, values);
		db.close();
	}

	/**
	 * Checks if the table allready exists if not it will return giving
	 * appropriate warning to main activity that it needs to create a new
	 * account.
	 * 
	 * @param dbHelper
	 *            the database helper used.
	 */
	public boolean load(final DatabaseHelper dbHelper) {
		Log.d(TAG, "Entering Database loading...");

		final SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor cursor;

		cursor = db.query(DatabaseHelper.ACCOUNT_TABLE_NAME, new String[] {
				DatabaseHelper.ACCOUNT_ID, DatabaseHelper.ACCOUNT_NAME,
				DatabaseHelper.ACCOUNT_GENDER, DatabaseHelper.ACCOUNT_XP,
				DatabaseHelper.ACCOUNT_LEVEL }, null, null, null, null, null);

		cursor.moveToFirst();

		if (cursor == null || cursor.getCount() <= 0) {
			Log.d(TAG, "Table " + DatabaseHelper.ACCOUNT_TABLE_NAME
					+ " doest not exist in the database");

			return false;
		} else {
			Log.d(TAG, "Table " + DatabaseHelper.ACCOUNT_TABLE_NAME
					+ " allready exists in the database");

			int xp;
			int level;

			this.setAccountName(cursor.getString(cursor
					.getColumnIndex(DatabaseHelper.ACCOUNT_NAME)));
			this.setGender(cursor.getString(cursor
					.getColumnIndex(DatabaseHelper.ACCOUNT_GENDER)));
			xp = cursor
					.getInt(cursor.getColumnIndex(DatabaseHelper.ACCOUNT_XP));
			level = cursor.getInt(cursor
					.getColumnIndex(DatabaseHelper.ACCOUNT_LEVEL));

			this.lvlHandler = new LevelHandler(level, xp);
		}

		cursor.close();

		return true;
	}

	/**
	 * Updates account information in the table.
	 * 
	 * @param dbHelper
	 *            the database helper used.
	 */
	public void update(final DatabaseHelper dbHelper) {
		final SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.ACCOUNT_LEVEL, this.getLevel());
		values.put(DatabaseHelper.ACCOUNT_XP, this.getXp());

		db.update(DatabaseHelper.ACCOUNT_TABLE_NAME, values, null, null);
		db.close();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(new String[] { Long.toString(this.id),
				this.accountName, this.gender });

		dest.writeInt(this.lvlHandler.getXp());
		dest.writeInt(this.lvlHandler.getLevel());
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public PubCrawlAccount createFromParcel(Parcel in) {
			return new PubCrawlAccount(in);
		}

		public PubCrawlAccount[] newArray(int size) {
			return new PubCrawlAccount[size];
		}
	};
}
