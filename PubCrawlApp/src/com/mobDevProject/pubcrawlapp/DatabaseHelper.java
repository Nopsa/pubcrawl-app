package com.mobDevProject.pubcrawlapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


class DatabaseHelper extends SQLiteOpenHelper 
{
	private static final String TAG = "DatabaseHelper";
	
	private static final String DATABASE_NAME = "PubCrawlDB.db";
	private static final int DATABASE_VERSION = 1;

	public static final String ACCOUNT_TABLE_NAME = "Account";
	public static final String CHALLENGE_TABLE_NAME = "Challenges";

	public static final String ACCOUNT_ID = "id";
	public static final String ACCOUNT_NAME = "accountName";
	public static final String ACCOUNT_GENDER = "gender";
	public static final String ACCOUNT_XP = "xp";
	public static final String ACCOUNT_LEVEL = "level";
	
	public static final String CHALLENGE_ID = "id";
	public static final String CHALLENGE_TITLE = "title";
	public static final String CHALLENGE_TEXT = "text";
	public static final String CHALLENGE_XP_GAIN = "xpGain";
	public static final String CHALLENGE_DIFFICULTY = "difficulty";
	public static final String CHALLENGE_GENDER_SPECIFIC = "genderSpecific";

	private static final String ACCOUNT_CREATE_TABLE = "CREATE TABLE "
			+ ACCOUNT_TABLE_NAME + " (" 
			+ ACCOUNT_ID + " INTEGER PRIMARY KEY," 
			+ ACCOUNT_NAME + " TEXT,"
			+ ACCOUNT_GENDER + " TEXT," 
			+ ACCOUNT_XP + " INTEGER," 
			+ ACCOUNT_LEVEL + " INTEGER" 
			+ ");";
	

	private static final String CHALLENGE_CREATE_TABLE = "CREATE TABLE "
			+ CHALLENGE_TABLE_NAME + " ("
			+ CHALLENGE_ID + " INTEGER PRIMARY KEY,"
			+ CHALLENGE_TITLE + " TEXT," 
			+ CHALLENGE_TEXT + " TEXT," 
			+ CHALLENGE_XP_GAIN + " INTEGER,"
			+ CHALLENGE_GENDER_SPECIFIC + " INTEGER,"
			+ CHALLENGE_DIFFICULTY + " INTEGER" + ");";
	
	
	/**
	 * Sets up db helper.
	 * @param ctx activity context
	 */
	public DatabaseHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(ACCOUNT_CREATE_TABLE);
		db.execSQL(CHALLENGE_CREATE_TABLE);
	}

	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO How did our DB change? Have we added new column? Renamed the column? 
		// Now - handle the change.
	}
	
	

}