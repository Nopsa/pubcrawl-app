package com.mobDevProject.pubcrawlapp;

import java.security.PublicKey;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;

public class MainActivity extends FragmentActivity 
						  implements DialogShareToFacebookFragment.FacebookDialogListener{


	private MainFragment mainFragment;
	private DialogShareToFacebookFragment dialogFragment;
	
	private static final String TAG = "MainActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		
		Log.d(TAG, "onCreate(Bundle savedInstanceState");
		
		if (savedInstanceState == null) {
	        // Add the fragment on initial activity setup
	        mainFragment = new MainFragment();
	        getSupportFragmentManager().beginTransaction().add(android.R.id.content, mainFragment).commit();
	    } else {
	        // Or set the fragment from restored state info
	        mainFragment = (MainFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
	    }
	
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void showNoticeDialog() {
        // Create an instance of the dialog fragment and show it
		dialogFragment = new DialogShareToFacebookFragment();
        dialogFragment.show(getSupportFragmentManager(), "DialogShareToFacebookFragment");
    }
	
	@Override
	public void onDialogPositiveClick(DialogFragment dialog) {
		// User touched the dialog's positive button
		// Publishes the challenge the user has done to the users facebook feed. 
		mainFragment.publishChallenge();
	}

	@Override
	public void onDialogNegativeClick(DialogFragment dialog) {
		// User touched the dialog's negative button
		// Does not share the challenge.
	}	
}