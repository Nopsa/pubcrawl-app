package com.mobDevProject.pubcrawlapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class AccountSetupActivity extends Activity {
	private static final String TAG = "AccountSetupActivity";

	private PubCrawlAccount account;

	private ImageButton imageButtonMaleGender;
	private ImageButton imageButtonFemaleGender;
	private EditText editTextUserName;
	private Button continueButton;

	private boolean genderChosen = false;;
	private boolean userNameEntered = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account_setup);

		Log.d(TAG, "Getting bundle");

		Bundle data = getIntent().getExtras();
		this.account = (PubCrawlAccount) data.getParcelable("pubCrawlAccount");

		Log.d(TAG,
				"Entered AccountSetup Activity with\nAccount name: "
						+ account.getAccountName() + "\nAccount gender: "
						+ account.getGender() + "\n");

		this.enableInputListeners();
	}

	@Override
	public void onBackPressed() {

	}

	/**
	 * Return to MainFragment carrying the created account.
	 */
	private void endActivity() {
		Intent returnIntent = new Intent();
		returnIntent.putExtra("account", this.account);
		setResult(RESULT_OK, returnIntent);
		finish();
	}

	/**
	 * Hides the keyboard.
	 */
	private void hideKeyboard() {
		InputMethodManager inputManager = (InputMethodManager) getBaseContext()
				.getSystemService(getBaseContext().INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(this.getCurrentFocus()
				.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	/**
	 * Enables the continue button if gender has been chosen and a user name is
	 * entered.
	 */
	private void tryToEnableContinueButton() {
		if (this.genderChosen && this.userNameEntered) {
			this.continueButton.setEnabled(true);
		}
	}

	/**
	 * Create the edit text for entering user name. Setup listeners for male and
	 * female image button. Setup continue button.
	 */
	private void enableInputListeners() {
		this.editTextUserName = (EditText) findViewById(R.id.account_name);
		editTextUserName
				.setOnEditorActionListener(new OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							if (!AccountSetupActivity.this.editTextUserName
									.getText().toString().matches("")) {
								Log.d(TAG,
										AccountSetupActivity.this.editTextUserName
												.getText().toString());

								AccountSetupActivity.this.userNameEntered = true;
								AccountSetupActivity.this
										.tryToEnableContinueButton();

								AccountSetupActivity.this.account
										.setAccountName(AccountSetupActivity.this.editTextUserName
												.getText().toString());

								AccountSetupActivity.this.hideKeyboard();
							} else {
								return false;
							}
						}

						return true;
					}
				});

		this.imageButtonMaleGender = (ImageButton) findViewById(R.id.image_gender_male);
		this.imageButtonMaleGender.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				AccountSetupActivity.this.account.setGender("male");

				if (!AccountSetupActivity.this.editTextUserName.getText()
						.toString().matches("")) {
					AccountSetupActivity.this.userNameEntered = true;
					AccountSetupActivity.this.tryToEnableContinueButton();
					AccountSetupActivity.this.account
							.setAccountName(AccountSetupActivity.this.editTextUserName
									.getText().toString());
				}

				AccountSetupActivity.this.genderChosen = true;
				AccountSetupActivity.this.tryToEnableContinueButton();

				if (AccountSetupActivity.this.imageButtonFemaleGender
						.isSelected()) {
					AccountSetupActivity.this.imageButtonFemaleGender
							.setSelected(false);
				}

				AccountSetupActivity.this.imageButtonMaleGender
						.setSelected(true);

				AccountSetupActivity.this.hideKeyboard();
			}
		});

		this.imageButtonFemaleGender = (ImageButton) findViewById(R.id.image_gender_female);
		this.imageButtonFemaleGender.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				AccountSetupActivity.this.account.setGender("female");

				if (!AccountSetupActivity.this.editTextUserName.getText()
						.toString().matches("")) {
					AccountSetupActivity.this.userNameEntered = true;
					AccountSetupActivity.this.tryToEnableContinueButton();
					AccountSetupActivity.this.account
							.setAccountName(AccountSetupActivity.this.editTextUserName
									.getText().toString());
				}

				AccountSetupActivity.this.genderChosen = true;
				AccountSetupActivity.this.tryToEnableContinueButton();

				if (AccountSetupActivity.this.imageButtonMaleGender
						.isSelected()) {
					AccountSetupActivity.this.imageButtonMaleGender
							.setSelected(false);
				}

				AccountSetupActivity.this.imageButtonFemaleGender
						.setSelected(true);

				AccountSetupActivity.this.hideKeyboard();
			}
		});

		this.continueButton = (Button) findViewById(R.id.button_continue);
		this.continueButton.setEnabled(false);

		this.continueButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				AccountSetupActivity.this.endActivity();

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.account_setup, menu);
		return true;
	}

}
