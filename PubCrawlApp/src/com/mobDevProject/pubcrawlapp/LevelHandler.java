package com.mobDevProject.pubcrawlapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public class LevelHandler implements Parcelable {
	private static final String TAG = "LevelHandler";

	private int xp = 0;
	private int level = 0;
	private int xpNeededToLevelUp = 0;

	public LevelHandler() {
		Log.d(TAG, "LevelHandler()");
		Log.d(TAG, "Exiting LevelHandler()");
	}

	public LevelHandler(int level, int xp) {
		this.level = level;
		this.xp = xp;
		calculateXpNeededToLevelUp();
	}

	public LevelHandler(Parcel parcel) {
		this.xp = parcel.readInt();
		this.level = parcel.readInt();
		this.xpNeededToLevelUp = parcel.readInt();
	}

	/**
	 * Calculates how much xp is needed to level up.
	 * 
	 * TODO Make a better scaling.
	 */
	private void calculateXpNeededToLevelUp() {
		this.xpNeededToLevelUp = this.level * 50;
	}

	private void levelUp() {
		this.level += 1;
	}

	public void addXp(int xp) {
		int xpHolder;
		xpHolder = this.xp + xp;

		if (xpHolder >= this.xpNeededToLevelUp) {
			// Get the remainding xp from a level up.
			int xpRemaindingAfterLevelUp = xpHolder - this.xpNeededToLevelUp;

			this.levelUp();
			this.resetXp();
			this.calculateXpNeededToLevelUp();
			this.addXp(xpRemaindingAfterLevelUp);
		} else {
			this.xp += xp;
		}
	}

	public void resetXp() {
		this.xp = 0;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getXp() {
		return this.xp;
	}

	public int getLevel() {
		return this.level;
	}

	public int getRequiredXpToLevel() {
		return this.xpNeededToLevelUp;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int flags) {
		parcel.writeInt(this.xp);
		parcel.writeInt(this.level);
		parcel.writeInt(this.xpNeededToLevelUp);
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public LevelHandler createFromParcel(Parcel in) {
			return new LevelHandler(in);
		}

		public LevelHandler[] newArray(int size) {
			return new LevelHandler[size];
		}
	};
}
