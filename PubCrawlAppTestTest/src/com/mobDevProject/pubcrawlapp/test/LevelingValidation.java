package com.mobDevProject.pubcrawlapp.test;

import com.mobDevProject.pubcrawlapp.LevelHandler;

import junit.framework.TestCase;

public class LevelingValidation extends TestCase 
{
	private LevelHandler levelHandler = null;
	private int expectedXp = 0;
	private int expectedLevel = 0;
	
	static final int XP_MULTIPLIER = 50;

	
	protected void setUp() throws Exception 
	{
		super.setUp();
		
		this.levelHandler = new LevelHandler(1, 0);
	}
	

	protected void tearDown() throws Exception 
	{
		super.tearDown();
	}
	
	
	/**
	 * Try to give some xp but not level up.
	 */
	public void testAddXp()
	{
		this.expectedLevel = 1;
		this.expectedXp = 25;
		
		this.levelHandler.addXp(25);
		
		assertEquals(this.expectedLevel, this.levelHandler.getLevel());
		assertEquals(this.expectedXp, this.levelHandler.getXp());
	}
	
	
	/**
	 * Try to jump from level 1 to level 3.
	 */
	public void testMultipleLevelUp()
	{
		this.expectedLevel = 3;
		this.expectedXp = 0;
		
		this.levelHandler.addXp(XP_MULTIPLIER * 3);
		
		assertEquals(this.expectedLevel, this.levelHandler.getLevel());
		assertEquals(this.expectedXp, this.levelHandler.getXp());
	}
	
	
	/**
	 * Try to level from 1 to 50.
	 */
	public void testLeveling()
	{
		this.expectedXp = 0;
		
		int levelUp = 0;
		
		for(int level = 1; level < 50; level++)
		{
			this.levelHandler.addXp(XP_MULTIPLIER * level);
			
			levelUp = level + 1;
			this.expectedLevel = levelUp;
			
			assertEquals(this.expectedLevel, this.levelHandler.getLevel());
			assertEquals(this.expectedXp, this.levelHandler.getXp());
		}
	}
}
